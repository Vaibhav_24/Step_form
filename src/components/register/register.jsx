import React, { useState, useEffect } from 'react';

const Register = () => {
  const [currentTab, setCurrentTab] = useState(0);

  useEffect(() => {
    showTab(currentTab);
  }, [currentTab]);

  const showTab = (n) => {
    const x = document.getElementsByClassName("tab");
    for (let i = 0; i < x.length; i++) {
      x[i].style.display = "none";
    }
    x[n].style.display = "block";

    const prevBtn = document.getElementById("prevBtn");
    const nextBtn = document.getElementById("nextBtn");
    if (n === 0) {
      prevBtn.style.display = "none";
    } else {
      prevBtn.style.display = "inline";
    }
    if (n === (x.length - 1)) {
      nextBtn.innerHTML = "Submit";
    } else {
      nextBtn.innerHTML = "Next";
    }
    fixStepIndicator(n);
  };

  const nextPrev = (n) => {
    const x = document.getElementsByClassName("tab");
    if (n === 1 && !validateForm()) return false;
    x[currentTab].style.display = "none";
    let newTab = currentTab + n;
    if (newTab >= x.length) {
      document.getElementById("regForm").submit();
      return false;
    }
    setCurrentTab(newTab);
  };

  const validateForm = () => {
    const x = document.getElementsByClassName("tab");
    const y = x[currentTab].getElementsByTagName("input");
    let valid = true;

    for (let i = 0; i < y.length; i++) {
      if (y[i].value === "") {
        y[i].className += " invalid";
        valid = false;
      }
    }

    if (valid) {
      document.getElementsByClassName("step")[currentTab].className += " finish";
    }
    return valid;
  };

  const fixStepIndicator = (n) => {
    const x = document.getElementsByClassName("step");
    for (let i = 0; i < x.length; i++) {
      x[i].className = x[i].className.replace(" active", "");
    }
    x[n].className += " active";
  };

  return (
    <div className="wrapper">
      <div className="container">
        <form method="post" action="action.php" id="regForm">
          <h1>Register:</h1>

          <div className="tab name">Name:
            <p><input placeholder="First name..." className="fname" onInput={(e) => e.target.className = ''} name="fname" /></p>
            <p><input placeholder="Last name..." className="lname" onInput={(e) => e.target.className = ''} name="lname" /></p>
          </div>
          <div className="tab contact">Contact Info:
            <p><input placeholder="E-mail..." className="email" onInput={(e) => e.target.className = ''} name="email" type="email" /></p>
            <p><input placeholder="Password..." className="pass" onInput={(e) => e.target.className = ''} name="pass" type="password" /></p>
            <p><input placeholder="Phone..." className="ph_number" onInput={(e) => e.target.className = ''} name="ph_number" type="number" /></p>
          </div>
          <div className="tab dob">Birthday:
            <p><input placeholder="dd" className="dd" onInput={(e) => e.target.className = ''} name="dd" type="number" min="1" max="31" /></p>
            <p><input placeholder="mm" className="mm" onInput={(e) => e.target.className = ''} name="mm" type="number" min="1" max="12" /></p>
            <p><input placeholder="yyyy" className="yy" onInput={(e) => e.target.className = ''} name="yy" type="number" min="1990" max="2019" /></p>
          </div>
          <div className="tab login">Login Info:
            <p><input placeholder="Username..." className="login_name" onInput={(e) => e.target.className = ''} name="login_name" type="text" /></p>
            <p><input placeholder="Password..." className="login_pass" onInput={(e) => e.target.className = ''} name="login_pass" type="password" /></p>
          </div>
          <div style={{ overflow: "auto" }}>
            <div style={{ float: "right" }}>
              <button type="button" id="prevBtn" onClick={() => nextPrev(-1)}>Previous</button>
              <button type="button" id="nextBtn" onClick={() => nextPrev(1)}>Next</button>
            </div>
          </div>

          <div style={{ textAlign: "center", marginTop: "40px" }}>
            <span className="step">1</span>
            <span className="step">2</span>
            <span className="step">3</span>
            <span className="step">4</span>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Register;