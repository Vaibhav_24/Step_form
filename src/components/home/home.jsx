import React from 'react'

const home = () => {
  return (
    <div>  <div className="container">
    <h1>Welcome to Landing Stars</h1>
    <p>Explore the beauty of dynamic backgrounds.</p>
    <a href="#features" className="cta-button">Learn More</a>
  </div>
  <div className="container">
    <h1>Features</h1>
    <div className="features">
      <div className="feature-item">
        <h2>Feature 1</h2>
        <p>Detail about Feature 1.</p>
      </div>
      <div className="feature-item">
        <h2>Feature 2</h2>
        <p>Detail about Feature 2.</p>
      </div>
      <div className="feature-item">
        <h2>Feature 3</h2>
        <p>Detail about Feature 3.</p>
      </div>
    </div>
    <div className="logos">
      <img src="https://via.placeholder.com/100" alt="Logo 1"/>
      <img src="https://via.placeholder.com/100" alt="Logo 2" />
      <img src="https://via.placeholder.com/100" alt="Logo 3" />
      <img src="https://via.placeholder.com/100" alt="Logo 4"/>
    </div>
  </div>
  <div className="container">
    <h1>Image Slider</h1>
    <div className="slider">
      <div className="slides">
        <div className="slide"><img src="https://via.placeholder.com/800x400" alt="Slide 1" /></div>
        <div className="slide"><img src="https://via.placeholder.com/800x400" alt="Slide 2" /></div>
        <div className="slide"><img src="https://via.placeholder.com/800x400" alt="Slide 3" /></div>
      </div>
      <div className="slider-buttons">
        <button className="slider-button" id="prevBtn">&#10094;</button>
        <button className="slider-button" id="nextBtn">&#10095;</button>
      </div>
    </div>
  </div>
  <div className="particles"></div>
  <div className="star-background"></div></div>
  )
}

export default home