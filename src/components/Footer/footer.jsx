import React from 'react'

const footer = () => {
  return (
   

    <footer className="footer ftco-section">
      <div className="container less-margin">
        <div className="row">
          {/* <div className="col-md-6 col-lg-3 mb-4 mb-md-0">
            <h2 className="footer-heading">Cleaning Programme of School or Institute</h2>
            <p>It is our foremost duty to keep our surroundings neat & clean. All the students are supposed to continue
              with good practices for hygiene and sanitation in school as well as at home. Hand washing with soap, use of
              mask & social distancing should be our priority </p>
            <ul className="ftco-footer-social list-unstyled float-md-left float-lft mt-4">
              <li className="ftco-animate"><a href="#"><span className="fa fa-twitter"></span></a></li>
              <li className="ftco-animate"><a href="#"><span className="fa fa-facebook"></span></a></li>
              <li className="ftco-animate"><a href="#"><span className="fa fa-instagram"></span></a></li>
            </ul>
          </div> */}
          {/* <div className="col-md-6 col-lg-3 mb-4 mb-md-0">
            <h2 className="footer-heading">Latest News</h2>
            <div className="block-21 mb-4 d-flex">
              <a className="img mr-4 rounded"  ></a>
              <div className="text">
                <h3 className="heading"><a href="#">Even the all-powerful Pointing has no control about</a></h3>
                <div className="meta">
                  <div><a href="#"><span className="icon-calendar"></span> Mar. 04, 2020</a></div>
                  <div><a href="#"><span className="icon-person"></span> Admin</a></div>
                  <div><a href="#"><span className="icon-chat"></span> 19</a></div>
                </div>
              </div>
            </div>
            <div className="block-21 mb-4 d-flex">
              <a className="img mr-4 rounded"></a>
              <div className="text">
                <h3 className="heading"><a href="#">Even the all-powerful Pointing has no control about</a></h3>
                <div className="meta">
                  <div><a href="#"><span className="icon-calendar"></span> Mar. 04, 2020</a></div>
                  <div><a href="#"><span className="icon-person"></span> Admin</a></div>
                  <div><a href="#"><span className="icon-chat"></span> 19</a></div>
                </div>
              </div>
            </div>
          </div> */}
          <div className="col-md-6 col-lg-6 ">
            <h2 className="footer-heading">Quick Links</h2>
            <ul className="list-unstyled">
              <li><a href="#" className="py-1 d-block">Home</a></li>
              <li><a href="#" className="py-1 d-block">About</a></li>
              <li><a href="#" className="py-1 d-block">Services</a></li>
              <li><a href="#" className="py-1 d-block">Works</a></li>
              <li><a href="#" className="py-1 d-block">Blog</a></li>
              <li><a href="#" className="py-1 d-block">Contact</a></li>
            </ul>
          </div>
          <div className="col-md-6 col-lg-6 ">
            <h2 className="footer-heading">Have a Questions?</h2>
            <div className="block-23 mb-3">
              <ul>
                <li><span className="icon fa fa-map-marker"></span><span class="text"> Jaipuria School of Bussiness, Ghaziabad, Uttar Pradesh</span></li>
                <li><a href="#"><span className="icon fa fa-phone"></span><span className="text">6263802189</span></a></li>
                <li><a href="#"><span className="icon fa fa-paper-plane"></span><span className="text">info@yourdomain.com</span></a></li>
              </ul>
            </div>
          </div>
        </div>
     
      </div>
    </footer>
 
  )
}

export default footer