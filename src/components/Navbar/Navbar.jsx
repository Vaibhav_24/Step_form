// import React from 'react'
// import { Link } from 'react-router-dom';


// const Navbar = () => {

//   return (
//     <div>
//          <div className="navbar" id="navbar">
//     <div className="logo">
//     <Link to="/"><a href=""  style={{fontSize:"20px"}}>Landing Stars</a></Link>
//     </div>
//     <div className="nav-links">
//       <Link to="/"><a href="">Home</a></Link>
//       <Link to="/"><a href="">About</a></Link>
//       <Link to="/"><a href="">Services</a></Link>
//       <Link to="/"><a href="">Contact</a></Link>
//       <Link to="/register"><a href="">Register</a>
//       </Link>
//     </div>
//   </div>


//   </div>
//   )
// }

// export default Navbar



import React from 'react';
import { Link } from 'react-router-dom';

const Navbar = () => {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container-fluid">
        <Link to="/" className="navbar-brand" style={{ fontSize: '20px' }}>Landing Stars</Link>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav ml-auto">
            <li className="nav-item">
              <Link to="/" className="nav-link">Home</Link>
            </li>
            <li className="nav-item">
              <Link to="/" className="nav-link">About</Link>
            </li>
            <li className="nav-item">
              <Link to="/" className="nav-link">Services</Link>
            </li>
            <li className="nav-item">
              <Link to="/" className="nav-link">Contact</Link>
            </li>
            <li className="nav-item">
              <Link to="/register" className="nav-link">Register</Link>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Navbar;



